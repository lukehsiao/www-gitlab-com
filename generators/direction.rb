require 'active_support/cache'
require 'active_support/core_ext/date/calculations'
require 'active_support/core_ext/numeric/time'
require 'active_support/notifications'

require 'faraday_middleware'
require 'faraday_middleware/parse_oj'

module GitlabParamsEncoder
  extend self

  def encode(hash)
    # The GitLab API does _not_ handle encoded `+` properly in label names
    Faraday::NestedParamsEncoder.encode(hash).gsub('%2B', '+')
  end

  def decode(string)
    Faraday::NestedParamsEncoder.decode(string)
  end
end

class GitLabInstance
  attr_reader :name

  def initialize(name)
    raise 'PRIVATE_TOKEN required to generate direction page' unless ENV['PRIVATE_TOKEN']

    @connection = Faraday.new(
      url: endpoint,
      headers: headers,
      request: { params_encoder: GitlabParamsEncoder, timeout: 90 }
    ) do |config|
      config.request :json
      config.response :logger, nil, { headers: false, bodies: false }
      config.response :oj
      config.adapter Faraday.default_adapter
    end

    @name = name

    cache_store.clear if ENV.key?('CLEAR_DIRECTION_CACHE')
  end

  def get(path, params = {})
    cache_key = @connection.build_url(path, params).to_s

    response = cache_store.read(cache_key)

    # Unless the cached response exists and was successful, perform the request
    if response.nil? || response.status != 200
      begin
        response = @connection.get(path, params)

        if response.status == 200
          cache_store.write(cache_key, response)

          if response.headers['X-Total-Pages'].to_i > 1
            warn "WARNING: More than one page returned by #{response.env.url}"
          end
        else
          warn "Error in retrieving URL #{response.env.url}: #{response.status}"

          return []
        end
      rescue Faraday::ClientError => ex
        warn "Error in retrieving URL #{ex.inspect}"

        return []
      end
    end

    response.body
  end

  private

  def endpoint
    'https://gitlab.com/api/v4'
  end

  def headers
    {
      'PRIVATE-TOKEN' => ENV['PRIVATE_TOKEN'],
      'User-Agent' => 'www-gitlab-com'
    }
  end

  def cache_store
    @cache_store ||= ActiveSupport::Cache::FileStore.new(
      File.expand_path('../tmp/cache/direction', __dir__),
      expires_in: 24.hours.to_i
    )
  end
end

class GitLabProject
  def initialize(id, instance)
    @id = id.gsub('/', '%2F')
    @instance = instance
    @name = instance.name
  end

  def milestones
    result = @instance.get("projects/#{@id}/milestones", state: 'active')
    result = result.select { |ms| ms['due_date'] }
    result.sort_by! do |ms|
      Date.parse ms['due_date']
    end
  end

  def milestone(milestone_id)
    @instance.get("projects/#{@id}/milestones/#{milestone_id}")
  end

  def milestone_direction_issues(milestone_id, labels)
    # labels parameter allows for additional filtering
    if labels.length > 1
      # we're fetching more than one label, so do not constrain filter. if we had boolean filters
      # this would be a normal AND query instead of an exception
      @instance.get("projects/#{@id}/issues", milestone: milestone_id, labels: 'direction')
    else
      # because we're just fetching one label, only get items containing that label
      @instance.get("projects/#{@id}/issues", milestone: milestone_id, labels: 'direction,' + labels.join(','))
    end
  end

  def wishlist_issues(label, not_label = nil)
    result = @instance.get("projects/#{@id}/issues", milestone: 'No+Milestone', labels: "direction,#{label}", state: 'opened', per_page: 100, sort: 'asc')
    result += @instance.get("projects/#{@id}/issues", milestone: 'Backlog', labels: "direction,#{label}", state: 'opened', per_page: 100, sort: 'asc')
    result = result.select { |issue| (issue['labels'] & not_label).empty? } if not_label
    result
  end

  def product_vision_issues(label, not_label = nil)
    result = @instance.get("projects/#{@id}/issues", labels: "Product+Vision+2018,#{label}", per_page: 100, sort: 'asc')
    result = result.select { |issue| (issue['labels'] & not_label).empty? } if not_label
    result
  end

  def tier_issues(tier)
    @instance.get("projects/#{@id}/issues", labels: "direction,#{tier}", state: 'opened', per_page: 100, sort: 'asc')
    # result = result.select { |issue| issue["milestone"] && issue["milestone"]["due_date"] }
  end

  def name
    project['name']
  end

  def web_url
    project['web_url']
  end

  def project
    @project ||= @instance.get("projects/#{@id}")
  end
end

class GitLabGroup
  def initialize(id, instance)
    @id = id
    @instance = instance
    @name = instance.name
  end

  def milestones
    result = @instance.get("groups/#{@id}/milestones", state: "active")
    result = result.select { |ms| ms['due_date'] }
    result.sort_by! do |ms|
      Date.parse ms['due_date']
    end
  end

  def product_vision_epics(label, not_label = nil)
    result = @instance.get("groups/#{@id}/epics", labels: "Product+Vision+2018,#{label}", per_page: 100, sort: 'asc')
    result = result.select { |epic| (epic['labels'] & not_label).empty? } if not_label
    result
  end

  def group
    @group ||= @instance.get("groups/#{@id}")
  end
end

def issue_bullet(issue)
  output = "- [#{issue['title']}](#{issue['web_url']})"
  output << ' <kbd>Starter</kbd>' if issue['labels'].include? 'GitLab Starter'
  output << ' <kbd>Premium</kbd>' if issue['labels'].include? 'GitLab Premium'
  output << ' <kbd>Ultimate</kbd>' if issue['labels'].include? 'GitLab Ultimate'
  output << ' <kbd>2018 Vision</kbd>' if issue['labels'].include? 'Product Vision 2018'
  output << ' <kbd>2019 Vision</kbd>' if issue['labels'].include? 'Product Vision 2019'
  output << ' <kbd>2020 Vision</kbd>' if issue['labels'].include? 'Product Vision 2020'
  output << "\n"
  output
end

def tier_bullet(issue)
  output = "- [#{issue['title']}](#{issue['web_url']})"
  output << " <kbd>#{issue['milestone']['title']}</kbd>" if issue['milestone']
  output << "\n"
  output
end

def product_vision_bullet(issue)
  output = "<i class=\"vision-item far fa-#{'check-' if issue['state'] == 'closed'}square\" aria-hidden=\"true\"></i> [#{issue['title']}](#{issue['web_url']})"
  output << ' <kbd>Starter</kbd>' if issue['labels'].include? 'GitLab Starter'
  output << ' <kbd>Premium</kbd>' if issue['labels'].include? 'GitLab Premium'
  output << ' <kbd>Ultimate</kbd>' if issue['labels'].include? 'GitLab Ultimate'
  output << "<br>\n"
  output
end

def epic_web_url(group, epic)
  "#{group.group['web_url']}/-/epics/#{epic['iid']}"
end

def product_vision_bullet_from_epic(group, epic)
  output = "<i class=\"vision-item far fa-minus-square\" aria-hidden=\"true\"></i> [#{epic['title']}](#{epic_web_url(group, epic)})"
  output << ' <kbd>Starter</kbd>' if epic['labels'].include? 'GitLab Starter'
  output << ' <kbd>Premium</kbd>' if epic['labels'].include? 'GitLab Premium'
  output << ' <kbd>Ultimate</kbd>' if epic['labels'].include? 'GitLab Ultimate'
  output << "<br>\n"
  output
end

def generate_direction(stages)
  # stages contains list like: stages = %w[manage plan create verify package release configure monitor secure]
  puts 'Generating direction for ' + stages.join(', ') + ' ...'
  direction_output = ''

  milestones = gitlaborg.milestones
  milestones.sort_by! do |ms|
    Date.parse ms['due_date']
  end

  milestones.each do |ms|
    next unless ms['due_date'] && Date.parse(ms['due_date']) >= Date.today

    issues = []

    edition.each do |project|
      issues += project.milestone_direction_issues(ms['title'], stages.map { |n| "devops:" + n })
    end

    next if issues.empty?

    # check if the release number can cast to float (i.e., 11.1, 11.12, etc.)
    # this will be used to not display a date for non-matching, but included
    # milestones such as "Next 3-4 releases"
    begin
      if Float(ms['title'])
        # was able to cast to float, looks like a normal release number
        output = "### #{ms['title']} (" + Date.parse(ms['due_date']).strftime('%Y-%m-%d') + ")\n\n"
      end
    rescue ArgumentError
      # milestone title is not a normal release number, do not include extra date info
      output = "### #{ms['title']}\n\n"
    end

    direction_output << output

    # run through all issues and throw in relevant bucket (stage)
    buckets = {}
    issues.each do |issue|
      stages.each do |stage|
        buckets[stage] = [] if buckets[stage].nil?
        buckets[stage] << issue if issue['labels'].include?("devops:#{stage}")
      end
    end

    # Now unfold the bins in order, so all the issues are under their stage
    stages.each do |stage|
      next unless buckets[stage].count.positive?

      if stages.length > 1
        # If there is only one potential stage to include, the stage name is not necessary to enumerate.
        direction_output << "#### #{stage.capitalize}\n"
      end
      buckets[stage].each do |issue|
        direction_output << issue_bullet(issue)
      end
      direction_output << "\n"
    end

    direction_output << "\n"
  end

  puts

  direction_output
end

def edition
  @edition ||= begin
                 com = GitLabInstance.new('GitLab.com')
                 ce = GitLabProject.new('gitlab-org/gitlab-ce', com)
                 ee = GitLabProject.new('gitlab-org/gitlab-ee', com)
                 omnibus = GitLabProject.new('gitlab-org/omnibus-gitlab', com)
                 runner = GitLabProject.new('gitlab-org/gitlab-runner', com)
                 pages = GitLabProject.new('gitlab-org/gitlab-pages', com)
                 [ce, ee, omnibus, runner, pages]
               end
end

def gitlaborg
  @gitlaborg ||= begin
               com = GitLabInstance.new('GitLab.com')
               GitLabGroup.new('gitlab-org', com)
             end
end

def label_list(label, exclude: nil, editions: nil)
  output = ''

  editions = edition if editions.nil?

  editions.each do |project|
    issues = project.wishlist_issues(label, exclude)
    issues.each do |issue|
      output << issue_bullet(issue)
    end
  end
  output = "No current issues\n" if output.empty?
  output
end

def tier_list(label)
  output = ''
  issues = []

  edition.each do |project|
    issues += project.tier_issues(label)
  end
  issues.sort_by! do |issue|
    if issue.dig('milestone', 'due_date')
      Date.parse(issue['milestone']['due_date'])
    else
      Date.new(2050, 1, 1)
    end
  end
  issues.each do |issue|
    output << tier_bullet(issue)
  end

  output = "No current issues\n" if output.empty?
  output
end

def product_vision_list(label, not_label = nil, editions = nil)
  output = ''

  epics = gitlaborg.product_vision_epics(label, not_label)
  epics.each do |epic|
    output << product_vision_bullet_from_epic(gitlaborg, epic)
  end

  editions = edition if editions.nil?

  editions.each do |project|
    issues = project.product_vision_issues(label, not_label)
    issues.each do |issue|
      output << product_vision_bullet(issue)
    end
  end
  output = "No issues\n" if output.empty?
  output
end

def generate_wishlist
  puts 'Generating wishlist...'
  output = {}

  # 'boards',
  # 'burndown charts',
  # 'capacity planning',
  # 'chat commands',
  # 'code review',
  # 'container registry',
  # 'deliver',
  # 'epics',
  # 'issue boards',
  # 'issues',
  # 'jira',
  # 'labels',
  # 'milestones',
  # 'major wins',
  # 'moderation',
  # 'notifications',
  # 'open source',
  # 'performance',
  # 'roadmaps',
  # 'search',
  # 'Monitoring',
  # 'search',
  # 'service desk',
  # 'todos',
  # 'usability',
  # 'vcs for everything',
  # 'wiki',

  [
    'devops:manage',
    'devops:plan',
    'devops:create',
    'devops:verify',
    'devops:package',
    'devops:release',
    'devops:configure',
    'devops:monitor',
    'devops:secure',
    'HA',
    'Cloud Native',
    'moonshots'
  ].each do |label|
    output[label] = label_list(label)
  end
  output['devops:distribution'] = label_list('direction', exclude: ['HA', 'Cloud Native'], editions: Array(edition[2]))

  ['GitLab Starter', 'GitLab Premium', 'GitLab Ultimate'].each do |tier|
    output[tier] = tier_list(tier)
  end

  puts

  output
end

def generate_product_vision
  puts 'Generating product vision...'
  output = {}

  [
    'boards',
    'project management',
    'portfolio management',
    'repository',
    'code review',
    'web ide',
    'license management',
    'devops:package',
    'devops:release',
    'application control panel',
    'infrastructure configuration',
    'issues',
    'operations',
    'feature management',
    'application performance monitoring',
    'infrastructure monitoring',
    'production monitoring',
    'error tracking',
    'logging',
    'performance',
    'Omnibus',
    'Cloud Native'
  ].each do |label|
    output[label] = product_vision_list(label)
  end
  output['ci'] = product_vision_list('devops:verify', ['Secure'])
  output['security testing'] = product_vision_list('devops:secure', ['license management'])

  puts

  output
end

if $PROGRAM_NAME == __FILE__
  generate_direction(%w[manage plan create verify package release configure monitor secure])
  generate_wishlist
  generate_product_vision
end
