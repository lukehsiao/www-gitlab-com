---
layout: job_family_page
title: "People Operations Generalist"
---

## Responsibilities

- Assist the Chief Culture Officer to develop the strategic direction for People Ops, and implement the functional steps to achieve those goals.
- Develop and implement HR policies, from recruitment and pay, to diversity and employee relations.
- Work with People Ops and the executive team to establish entities or co-employers in new countries as we scale. 
- Develop contracts and benefits programs for each entity 
- Manage co-employer relationships and act as the main point of contact
- Partner with Legal on international employment and contractual requirements
- Create People Operations processes and strategies following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Keep it efficient and DRY.
- Oversee all offboarding tasks and provide assistance when needed
- Suggest and implement improvements to People Operations, for example for performance reviews and various types of training.
- Process and manage Parental Leaves based on each entity benefit policy.
- Handle work permits/transfers for new hires and the current GitLab team, including the WBSO grant.
- Coordinate with Finance on payroll reporting.
- Process and manage the Tuition Reimbursement policy for GitLab.
- Collaborate with the HRBPs on Training Programs for Managers.
- Provide HR generalist support to HRBPs as and when needed
- Provide assistance in responding to emails in the people operations email and Slack channel

## Requirements

- Prior extensive experience in an HR or People Operations role
- Clear understanding of HR laws in one or multiple countries where GitLab is active
- Ability to work strange hours when needed (for example, to call an embassy in a different continent)
- Excellent written and verbal communication skills
- Exceptional customer service skills
- Strong team player who can jump in and support the team on a variety of topics and tasks
- Enthusiasm for and broad experience with software tools
- Proven experience quickly learning new software tools
- Willing to work with git and GitLab whenever possible
- Willing to make People Operations as open and transparent as possible
- Wanting to work for a fast moving startup
- You share our [values](/handbook/values), and work in accordance with those values
- The ability to work in a fast paced environment with strong attention to detail is essential.
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).
