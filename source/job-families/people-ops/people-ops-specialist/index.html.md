---
layout: job_family_page
title: "People Operations Specialist"
---

## Responsibilities

- First line of support for all People related questions through Slack
- Manage offboarding for all employees
- Order Supplies (Laptop, office equipment, business cards, etc.)
- Coordinate birthday and anniversary notifications
- Responsible for providing support for company wide use of Google, Slack, and Zoom (configuration, permissions, troubleshooting)
- Manage and Moderate Functional and Company team meetings
- Work closely with People Operations and broader People team to implement specific People Operations processes and transactions
- Provide extraordinary customer service to employees and leaders at Gitlab
- Maintain appropriate level of process, program, and policy knowledge in order to assist Employees
- Complete ad-hoc projects, reporting and tasks
- Proactively identify process inefficiencies and inconsistencies and collaborate towards an improved and more productive process that improves the employee and/or manager’s experience
- Assist with other People Operation activities as needed


## Requirements

- Stellar written and verbal communication skills
- Ability to function autonomously as well as in a team environment
- High sense of urgency
- Proven organizational skills with high attention to detail and the ability to prioritize.
- Confidence to learn new technologies (MAC, Google Suite, GitLab) and translate that learning to others. 
- Bachelors degree or 2 years related experience
- Experience at a growth-stage tech company is preferred
- Experience working in a data-driven environment is preferred
- Willing to work with git and GitLab whenever possible
- Willing to make People Operations as open and transparent as possible
- You share our [values](/handbook/values), and work in accordance with those values
- The ability to work in a fast paced environment with strong attention to detail is essential.

