---
layout: job_family_page
title: "Talent Coordinator"
---

This role reports to the Director of Recruiting and works daily to support the People Ops and Recruiting teams.

## Responsibilities

* Monitor the People Ops and Recruiting email aliases and Slack channels and assist team members and candidates with any questions or concerns they may have
* Monitor the [Team Call](/handbook/communication/#team-call) and agenda
* Monitor the [Group Conversations](/handbook/people-operations/group-conversations/)
* Consistently create GitLab blog posts with Group Conversations and upload Group Conversation videos to Youtube
* Conduct direct sourcing efforts to assist the recruiting team in identifying qualified candidates.
* Assist with keeping BambooHR (HRIS) up to date; upload documents, log assets, etc.
* Order equipment and supplies for team members, keeping in mind [the handbook guidelines](/handbook/spending-company-money/)
* Work with team members and Finance to keep track of equipment and supplies
* Assist the Recruiting team and hiring managers with [vacancies](/handbook/hiring/vacancies/)
* Assist the Recruiting team and new team members with [onboarding](/handbook/general-onboarding/)
* Assist the People Ops team with [offboarding](/handbook/offboarding/)
* Assist with candidate pipeline management in Greenhouse as needed
* Promote our values, culture and remote only passion
* Suggest improvements to People Ops and Recruiting policies, processes, and procedures
* Provide assistance to the People Ops and Recruiting teams with miscellaneous support tasks

## Requirements

* Minimum six months experience providing administrative support for a Human Resources or People Operations team
* Experience in a startup a plus
* Experience working remotely is a plus
* Proven ability to multitask and prioritize workload
* Excellent communication and interpersonal skills
* Desire to learn about Talent Acquisition and Human Resources
* Demonstrated ability to work in a team environment and work collaboratively across the organization
* Proficient in Google Suite
* Willingness to learn and use software tools including Git and GitLab, prior experience with GitLab is a plus.
* Organized, efficient, and proactive with a keen sense of urgency
* Ability to recognize and appropriately handle highly sensitive and confidential information
* Prior experience using an candidate tracking system (ATS), such as Greenhouse, is a plus
* Prior experience using a human resources information system (HRIS), such as BambooHR, is a plus
* Ability to recognize and appropriately handle highly sensitive and confidential information
* You share our [values](/handbook/values), and work in accordance with those values
* Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a 30 minute [screening call](handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 30 minute interview with our Recruiting Director.
* Next, candidates will be invited to schedule a 30 minute interview with at least one of our Recruiting team members.
* Candidates may be asked to schedule a 45 minute interview with our Chief Culture Officer.
* Finally, our CEO may choose to conduct a final interview.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
