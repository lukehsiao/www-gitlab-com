---
layout: job_family_page
title: "UX Researcher"
---

At GitLab, UX Researchers collaborate with our UX designers, front/backend engineers, product managers and the rest of the community to assist in determining what features should be built, how they should behave, and what the priorities should be. User Experience Researchers report to the UX Lead.

## Responsibilities

* Help to research and improve the user experience of GitLab.
* Help the company and the rest of the community build empathy for users by synthesizing and communicating user insights.
* Collaborate with our UX designers, front/backend engineers, product managers and the rest of the community to determine what features should be built, how they should behave, and what the priorities should be. This is mainly done on our public issue tracker.
* Create deliverables (reports, personas, customer journey maps, etc) to communicate research findings.
* Use quantitative data to complement qualitative data about users to inform company and product decisions.
* Respond to comments about GitLab's usability.
* Recruit and screen participants for user research that are willing to work in the open.
* Stay informed of the latest UX research techniques, tools and processes.

#### Junior UX Researcher

* Support the UX Researcher with scoping, designing and conducting research studies. This may include but not be limited to usability studies, user interviews, surveys and field studies.
* Conduct secondary research methods such as desk research, competitor analysis, etc.
* Help maintain GitLab's research panel.
* With support from the UX Researcher, interpret research findings and ensure UX Designers and Product Managers are debriefed on the results of our testing sessions.
* Work collaboratively with UX Designers and Product Managers to help identify opportunities for research.
* Assist in the creation of research deliverables such as reports, personas, customer journey maps, etc.

#### UX Researcher

* Develop and execute research plans and objectives based on an understanding of company’s strategic goals.
* Employ qualitative methods to understand user needs, motivations, and behaviors. This may include but not be limited to field studies, usability studies, diary studies, user interviews, and surveys.
* Maintain a participant database for user research recruiting.
* Write blog articles and respond to comments about GitLab's usability.

#### Senior UX Researcher

* Deeply understand the needs and issues of GitLab users.
* Propose, implement, and lead significant improvements based on research findings.
* Mentor more junior UX researchers.
* Delegate work to more junior UX researchers as appropriate.
* Have deep knowledge of GitLab UX research history and direction.
* Serve as the go-to person for UX research.
* Improve other UX researchers productivity through better tools and workflow.
* Collaborate with team members across the organization and advocate for users and user-centered solutions.
* Write blog posts and conduct [Group Conversations](/handbook/people-operations/group-conversations/) articulating UX research strategy.
* Interview potential UX research candidates.
* Have a deep understanding of UX research fundamentals.

## Tools

Tools used by UX Researchers at GitLab are flexible depending on the needs of the work.

## Success Criteria

You know you are doing a good job as a UX Researcher when:

* You are contributing research to resolve UX / UI issues assigned to milestones well before the milestone comes up.
* You are contributing ideas and solutions beyond existing issues.
* Users are overwhelmingly happy about your contributions.
* You collaborate effectively with Frontend Engineers, Developers, and Designers.

### UX Researcher Interview Questions <a name="ux-research-interview-questions"></a>

The UX Researcher Interview determines if a UX Researcher is a good fit for GitLab. Here are some questions we might ask:

1. What is your research process?
1. What are some existing case studies or research results we can see as an example of your work?
1. When you did `x` project, what was the biggest problem that you had to solve and how did you solve it?

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)
- [GitLab Design Kit](https://gitlab.com/gitlab-org/gitlab-design)
