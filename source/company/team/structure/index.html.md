---
layout: markdown_page
title: "Organizational Structure"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Organizational chart

You can see who reports to whom on our [organizational chart](/company/team/org-chart).

## Layers

| Level | Example | Peer group | Management group | Reporting group |
|--------------------------------|---------------------------------------|-----------------------|-------------------------------------|-----------------|
| Board member | [Chief Executive Officer](/job-families/chief-executive-officer/) | Board |  |
| Executive | [Chief Culture Officer](/job-families/people-ops/chief-culture-officer/) | Executives | [E-group](/handbook/leadership/#e-group) | Function |
| Senior Leader | [VP of Global Channels](/job-families/sales/vp-of-global-channels/) or Senior Director | Senior leaders | S-group |  |
| Director | [Director of Engineering](/job-families/engineering/engineering-management/#director-of-engineering) | Directors | [D-group](/handbook/leadership/#director-group) |  |
| Manager | [Engineering Manager](/job-families/engineering/engineering-management/#engineering-manager) | Managers | [M-group](/handbook/leadership/#management-group) | Team-members |
| Individual contributor (IC) | [Staff Developer](/job-families/engineering/developer/#staff-developer) | ICs |  | GitLabbers
| Community member | [Most Valuable Person](/community/mvp/) | Wider community |  |

The management group includes the levels above. The reporting group includes the roles below, although sometimes without the community member layer.

U-staff are the people attending a weekly meeting of the senior leaders and directs.

## Layers

GitLab Inc. has at most five layers in the company structure (IC, Manager, Director, Executives, CEO).

The exeuctive layer is structured as follows. There are two primary processes, product (product management and engineering) and go-to market (marketing and sales). Some companies have a Chief Product Officer (CPO) for the former and a Chief Operating Officer (COO) for the latter. We have a flatter organization. The C-level exec for product is the CEO and the VP's of Product management, Engineering, and Alliances report to the CEO. Marketing and sales have separate C-level execs. The two supporting functions, finance and people, also each have a C-level executive, the Chief Financial Officer (CFO) and Chief Culture Officer (CCO).

Some of individual contributors (without any direct reports) have manager in their title but are not considered a manager in our company structure structure nor salary calculator, examples are product manager, accounting manager, account manager, channel sales manager, technical account manager, field marketing managers, online marketing manager, and product marketing manager.

## Specialists, experts, and mentors

People can be a specialist in one thing and be an expert in multiple things. These are listed on the [team page](/company/team/).

### Specialist

Specialists carry responsibility for a certain topic.
They keep track of issues in this topic and/or spend the majority of their time there.
Sometimes there is a lead in this topic that they report to.
You can be a specialist in only one topic.
The specialist description is a paragraph in the job description for a certain title.
A specialist is listed after a title, for example: Developer, database specialist (do not shorten it to Developer, database).
Many specialties represent stable counterparts. For instance, a "Test Automation Engineer, Create" specializes in the "Create" [stage group](#stage-groups) and is dedicated to that group.
The if you can have multiple ones and/or if you don't spend the majority of your time there it is probably an [expertise](/company/team/structure/#expert).
Since a specialist has the same job description as others with the title they have the same career path and compensation.

### Expert

Expert means you have above average experience with a certain topic.
Commonly, you're expert in multiple topics after working at GitLab for some time.
This helps people in the company to quickly find someone who knows more.
Please add these labels to yourself and assign the merge request to your manager.
An expertise is not listed in a role description, unlike a [specialist](/job-families/specialist).

For Production Engineers, a listing as "Expert" can also mean that the individual
is actively [embedded with](/handbook/engineering/infrastructure/#embedded) another team.
Following the period of being embedded, they are experts in the regular sense
of the word described above.

Developers focused on Reliability and Production Readiness are named [Reliability Expert](/job-families/expert/reliability/).

### Mentor

Whereas an expert might assist you with an individual issue or problem, mentorship is about helping someone grow their career, functional skills, and/or soft skills. It's an investment in someone else's growth.

Some people think of expertise as hard skills (Ruby, International Employment Law, etc) rather than soft skills (managing through conflict, navigating career development in a sales organization, etc).

If you would like to be a mentor in a certain area, please add the information to the team page. It is important to note whether you would like to be a mentor internally and/or externally at GitLab. Examples of how to specify in the expertise section of the team page: `Mentor - Marketing, Internal to GitLab` or `Mentor - Development (Ruby), External and Internal to GitLab`.

## Crew

When we work together cross functionally we call that a **crew**. A crew is a temporary group, it disbands after work is complete. A crew is self-organizing, for example our product managers are not project managers that tell you what to do, and a cross functional team does not have a manager. A crew doesn't have reporting lines, we [don't want a matrix organization](/handbook/leadership/#no-matrix-organization). An example of a crew is the people working on our project to migrate GitLab.com to Google Cloud Platform who are from the production, build, database, and Geo groups.

## Stage Groups

Parts of our engineering organization are directly aligned with our [product categories](/handbook/product/categories/). For example we have a Product Manager, an Engineering Manager, Backend Developers, Frontend Developers, and UX Designers who are dedicated to the [stage](/handbook/product/categories/#hierarchy) named "Monitor". Collectively, these individuals form the "Monitor group". The word "Monitor" appears in their titles as a specialty, and in some cases, their team name. Similar to a crew, a DevOps stage group has no reporting lines because we [don't want a matrix organization](/handbook/leadership/#no-matrix-organization). Instead, we rely on stable counterparts to make a stage group function well.
