---
layout: markdown_page
title: Support
description: "Visit the GitLab support page to find product support links and to contact the support team."
---

## Scope of Support for GitLab Self-managed (Starter, Premium, Ultimate licenses)
GitLab Support will help troubleshoot all components bundled with GitLab Omnibus when used
as a packaged part of a GitLab installation. Any assitance with modifications to GitLab, including new functionality, bug-fixes
or other code changes should go through the GitLab [issue tracker](https://gitlab.com/gitlab-org/gitlab-ce/issues), triage, and release cycle. 

Support is not offered for local modifications to GitLab source code.

We understand that GitLab is often used in complex environments in combination with a variety of 
tools. We'll do best-effort support in debugging components that work alongside GitLab.

Unless otherwise specified in your support contract, we support the current major version
and previous two major versions only. As of writing, GitLab installations running versions 
in the 11.x, 10.x and 9.x series are eligible for support.

**Note**: If you obtained an Ultimate license as part of GitLab's [Open Source or Education programs](https://about.gitlab.com/2018/06/05/gitlab-ultimate-and-gold-free-for-education-and-open-source/),
Support (as included with Starter, Premium or Ultimate licenses) is **not** included unless purchased separately. Please see the [GitLab OSS License/Subscription Details](https://gitlab.com/gitlab-com/gitlab-oss/blob/master/README.md#licensesubscription-details) for additional details

### Outside of the Scope of Support for Self-managed Instances

| Out of Scope       | Example        | What's in-scope then?   |
|--------------------|----------------|-------------------------|
| 3rd party applications and integrations | *I can't get Jenkins to run builds kicked off by GitLab. Please help me figure out what is going on with my Jenkins server.* | GitLab Support can help ensure that GitLab is providing properly formatted data to 3rd party applications and integrations. |
| Debugging EFS problems | *GitLab is slow in my HA setup. I'm using EFS.* | EFS is **not** recommended for HA setups (see our [HA on AWS doc](https://docs.gitlab.com/ee/university/high-availability/aws/)).<br/><br/>GitLab Support can help verify that your HA setup is working as intended, but will not be able to investigate EFS backend storage issues. |
| Troubleshooting non-GitLab Omnibus components | *I'm trying to get GitLab to work with Apache, can you provide some pointers?* | GitLab Support will only assist with the specific components and versions that ship with the GitLab Omnibus package, and only when used as a part of a GitLab installation. | 
| Local modifications to GitLab | *We added a button to ring a bell in our office any time an MR was accepted, but now users can't log in.* | GitLab Support would direct you to create a feature request or submit a merge request for code review to incorporate your changes into the GitLab core. | 
| Old versions of GitLab | *I'm running GitLab 7.0 and X is broken.* | GitLab Support will invite you to upgrade your installation to a more current release. Only the current and two previous major versions are supported. |

Please also review the items outlined in the [Outside of the Scope of Support for all tiers](index.html#outside-of-the-scope-of-support-for-all-tiers) section.