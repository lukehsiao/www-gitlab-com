---
layout: markdown_page
title: "Sales Enablement"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Sales Enablement

### Training Playlist

You can watch previous [GitLab sales enablement training sessions](https://www.YouTube.com/watch?v=ZyyBq3_rzJo&list=PLFGfElNsQthYe-_LZdge1SVc1XEM1bQfG) on YouTube. Historical sessions that were recorded in Google drive can be accessed via this  [spreadsheet](https://docs.google.com/spreadsheets/d/1ETY7FfCzb2q9h2EkYttlW_Qpl7IHUF-F2rOJG2W03Yk/edit#gid=0).

#### Why we livestream

Both direct sales and channel sales training are part of the same playlist. We place the recordings of our training on YouTube so that everyone can access. We love everyone to contribute including:
- Direct Salespeople
- Channel Resellers
- Rest of company
- Customers
- Users
- Analysts
- Partners

#### Why we do private Q&A

We turn the recording off for Sales Enablement Questions and Answers. This is so that our sales team can ask confidential questions about our customers and in-flight deals.

### Enablement pages
- [GitLab CI/CD for GitHub FAQ](./github-ci-cd-faq/)
- [Cloud Native Ecosystem](./cloud-native-ecosystem/)
- [Enterprise IT Roles](../enterprise-it-roles/)

## Upcoming training
To see what training is coming soon view the [Sales Enablement issue board](https://gitlab.com/gitlab-com/marketing/general/boards/465497?=&label_name[]=Sales%20Enablement).

### To request new sales enablement sessions
- Create an issue in the [Marketing general issue](https://gitlab.com/gitlab-com/marketing/general/issues), tracker
- Label the issue with `Sales enablement`.
- @mention `@williamchia` in the issue description and he will coordinate with the product marketing team to prioritize and schedule the training.
- If you need a training prioritized @mention `@pmm-team` in the `#product-marketing` slack channel.

### Scheduling trainings
- The PMM team discusses upcoming trainings in the weekly PMM meeting.
- Sessions picked to execute on should be chosen from the backlog, or a new issue created, and moved to the `Sales Enablement Acceptedd` column.
- Assign the issue to the speaker and add `Moderator: <name>` to the issue description.
  - The speaker will then research and generate the conent for the training.
- Once the speaker is ready, a moderator and date should be chosen for the training.
  - Assign the moderator (in addition to the speaker) to the issue and add `Moderator: <name>` to the issue description.
  - Add a due date to the issue.
  - Add the date in ISO format to the issue title.
  - Move the issue to the `Sales Enablement - Scheduled` column.
  - Manually drag the issue to order issues in the column by date.

### Enablement calendar
Sales enablement sessions are scheduled on the [Product Marketing calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_5n3g60l58thum9aovp8iisav34%40group.calendar.google.com&ctz=America%2FLos_Angeles) so that everyone on the PMM team has the ability to edit the calendar event.

## How to conduct a sales enablement training

- Each training session has a [speaker](#speaker) and a [moderator](#moderator).
- Sessions are 30 min. long.
- The presentation portion should be 10 minutes leaving 20 minutes for Q&A.

### Speaker

- Create your content as a handbook page. (Don't use a slide deck.)
  - Create a new directory under `https://about.gitlab.com/handbook/marketing/product-marketing/enablement/` with the title of your talk.
    - For example: `/handbook/marketing/product-marketing/enablement/cloud-native-ecosystem/`.
  - Add an `index.html.md` file to that directory.
    - Use this template:

    ```
    ---
    layout: markdown_page
    title: "Title goes here"
    ---

    ## On this page
    {:.no_toc}

    - TOC
    {:toc}

    ## Title goes here
    ```

  - Add your content to this page.
  - Add links to any other pages you need to reference on this page so you can present from training page.
  - Add a link to the training page from this page in the [#enablement-pages](#enablement-pages) section.

### Moderator

The moderator should serve as the host of the call and overall wingperson for the presenter. The moderator monitors chat to raise questions to the presenter and searches for links that are mentioned on the call to make sure they get linked in the handbook page for the training.  

- Log in to zoom 5 minutes ahead of time.
- Pause the recording.
- At 9am PT welcome everyone to the call and remind them that the presentation will be recorded and then we'll stop the recording for Q&A.

> Hello and welcome to today's sales enablement session. As a reminder we'll be recording this session live and posting it to YouTube. We'll then pause the recording and answer live Q&A. With that I'll start the recording now.

- Start the recording.
- Introduce the speaker and topic.

> For today's GitLab sales enablement training we are pleased to have `<speaker name>` to talk to use about `<topic>`. With that I'd like to pass it over to `<speaker name>`

- Monitor the time. If the presentation goes longer than 15 minutes, interrupt to remind the speaker that we are at 15 min and we want to leave time for Q&A.
- Stop the recording.
- After the sales enablement session, follow up with Agnes to get the YouTube link is posted to YouTube.
- Make sure a link to the handbook page is linked in the description of the YouTube video.
- Post a link to the YouTube video in the #sales slack channel.

## XDR (BDR/SDR) Coaching
In order to provide frequent feedback and coaching to the BDR/SDR team, weekly coaching/enablement sessions are hosted where the XDR team is able to ask questions about specific topics they are encountering with their customers. These coaching sessions are driven based on demand from the team and are interactive to help grow skills, awareness and knowledge about the IT and software delivery challenges that enterprise customers are facing.

[XDR Coaching Notes/Topics](./xdr-coaching)

### Establishing the XDR Coaching program
#### First meeting - AMA to set the stage and get organized

**Objective:**

* Encourage the team open up and be comfortable asking questions.   
* Establish ongoing agenda and get inputs for specific topics.

#### Proposed Recurring Agenda:
* Pre-work (preparation - specific resource to read, objectives, questions
* 10 min - specific discussion topic  (PMM presents a topic)
* 19 min - interactive / q&a - (where XDRs share case study/situations)
* 1 min -  Next topic (prework/prep for next session)

#### Requesting future discussion topics:
To requst discussion topics and/or to see future topics see the [XDR Coaching issue board](https://gitlab.com/gitlab-com/marketing/general/boards/772948?&label_name[]=XDR-Coaching)

#### Potential Resources:
* (An overview of IT/SW delivery challenges:)[https://learn.techbeacon.com/tracks/business-leaders-guide-software-innovation]
* others tbd


