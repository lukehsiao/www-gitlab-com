---
layout: markdown_page
title: "Analyst Relations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Analyst relations at GitLab

Analyst relations (AR) is generally considered to be a corporate strategy, communications and marketing activity, but at GitLab, because of our mission that everyone can contribute, we view the analyst community as participants as well. The primary owner of Analyst Relations at GitLab is Product Marketing, as they are the conduit between the analysts and the internal company information.  

Industry analysts usually focus on corporate or organizational buyers and their needs rather than on those of individual developers. At GitLab we view them as a marketing focus for those markets. They amplify our message to a different group than our traditional developer communities, although there is overlap to some degree between those communities.

## How we interact with the analyst community

Examples of how we engage with analysts include:
- Schedule briefings where we update analysts on our product, our company, our future direction and strategy;
- Answer questions for analyst reports such as Gartner MQs or Forrester Waves that provide in-depth information on product features, customer references and company information;
- Use analyst reports such as Gartner MQs or Forrester Waves that feature GitLab to help enhance or clarify our story for customers and partners;
- Provide our analyst newsletter to update analysts on what GitLab is doing between briefings;
   - [GitLab Analyst Newsletter October 2018](https://docs.google.com/document/d/1MrAsx2UgIQjwbIEplrqvUvZajEzVRuKfZOGhU-qTExw/edit?usp=sharing)
- Schedule inquiries where analysts answer specific questions we have about products, markets, or strategies we want to understand better;
- Schedule consulting days for an extended dive with an analyst into products, markets, or strategies we are working on;
- Invite analysts to participate in webinars, speaking engagements, quotes for media, or other events where an analyst presence would be beneficial; and
- Hire analyst’s market research departments to help us create, run, and interpret survey research that helps us target markets or develop products optimally.

## Accessing analyst reports

Most analyst companies charge for access to their reports. 

-   If GitLab purchases reprint rights to a report, then that link will be available here, on the [Analyst Relations web page](https://about.gitlab.com/analysts/), and on the relevant product page. Reprint rights are the rights to share the link to the report - these generally last six months to one year.
-   GitLab maintains relationships with some analyst companies that provide us with access to some or all of their relevant research. These reports are for internal use only and sometimes limited to named individuals.  Those reports are generally kept in an internal [GitLab folder for analyst relations](https://drive.google.com/drive/u/0/folders/1oFmtmoXsbjMb6IuPIgIIZ-MG-tLfOjpw). If you are a GitLabber and you need access to a particular report, please reach out to [Analyst Relations](mailto:analysts@gitlab.com) and I'll help you find the research you need.

## GitLab thought leadership with the analysts

This section highlights areas where GitLab believes it can help drive the industry forward with our insight and knowledge. This is a space where we can shape and refine our thinking.

### The complete DevOps toolchain:

**What it is**

We are interested in seeing the emergency of a new category within DevOps that looks at the entire DevOps toolchain that includes everything from planning to monitoring, including analytics and security. We are exploring what to call this. Suggestions include:
- All-in-One DevOps
- DevOps Toolchain
- Complete DevOps

There are several companies we believe could be included in this space such as:
- GitLab
- Microsoft Azure DevOps
- Atlassian
- CodeStar
 
**Why this is the right thing to do**
 - **Tools Tax** The main argument for this is currently a discussion of a "tools tax" which is essentially the cost to manage the overhead of having multiple tools that need to be maintained and integrated.  Having one tool greatly simplifies this process and lowers costs. This addresses the cost to management for the tool

- **Developer productivity** This second argument looks at the average amount of work involved for a developer in having to work with and train with multiple tools.

- **Data Value Stream** This third argument discusses the value to the organization of the data being connected and what it can tell the organization.

## What the analysts are saying about GitLab

This section contains highlights from areas where the analysts have rated GitLab in comparison to other vendors in a particular space. The highlights and lessons learned are listed here.  For more information click the link to go to the page dedicated to that report.

##### [The Forrester New Wave™: Value Stream Management Tools, Q3 2018](https://about.gitlab.com/analysts/forrester-vsm/)
##### [The Gartner Magic Quadrant for Application Release Orchestration, 2018](https://about.gitlab.com/analysts/gartner-aro/)

## Analyst reports that can help you deepen your knowledge
As part of XDR and Sales Enablement, some analyst reports are educational - they can help you build your understanding of the market. We've collected some of those articles to share with you here. These are behind the firewall and for use of employees where they have access rights. If you have any questions on access, please contact [Analyst Relations](mailto:analysts@gitlab.com).

[The complete list of documents by topic can be found here.](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/sales-training/)


## Areas for expanded coverage

As GitLab grows and our product capabilities expand, we are engaging with more analysts in more companies, on a wider array of topics. The following is a list of topics and analyst companies appearing on our radar. The information here shows the date the report was published as well as the criteria and vendors considered for the report.


### Forrester:

##### Wave: Configuration Management Software for Infrastructure Automation, October 11, 2017
- Leaders: Puppet Enterprise, Chef Automate
- [Link to report](https://reprints.forrester.com/#/assets/2/675/'RES137964'/reports)
- Criteria for inclusion: Evaluated solutions had to provide a minimum set of capabilities, including deployment, configuration modeling, monitoring
and governance, and community support.
- Element configuration management tools will converge with CDRA - Forrester expectation

##### Wave: Modern Application Functional Test Automation Tools, December 5, 2016
- Leaders: Parasoft, IBM, Tricentis, HPE
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Modern+Application+Functional+Test+Automation+Tools+Q4+2016/-/E-RES123866)
- Criteria for inclusion: Cross-browser FTA and mobile testing capabilities, UI and API FTA capabilities

##### Wave: Static Application Security Testing, December 12, 2017
- Leaders: Synopsys, CA Veracode
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Static+Application+Security+Testing+Q4+2017/-/E-RES139431)
- Criteria for inclusion: a comprehensive, enterprise-class SAST tool, interest from Forrester clients
- Language support and depth of results are key differentiators.
- Companies traditionally used SAST tools late in the software development lifecycle (SDLC) to scan products for vulnerabilities in proprietary code. Now developers need early remediation advice throughout the SDLC.

##### Wave: Strategic Portfolio Management (SPM) Tools, September 20, 2017
- Leaders: AgileCraft, CA, ServiceNow
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Strategic+Portfolio+Management+Tools+Q3+2017/-/E-RES136707)
- Criteria for inclusion: Integration of strategic planning capabilities, including goals, assessment, decision support, validation, gap analysis, and collaboration; portfolio management capabilities such as alignment, integrated operational alignment, portfolio analytics and data security; usability capabilities.
- Planning disciplines are transitioning from annual to continuous; PPM is too narrow of a focus.
- SPM tools support planning at a unified level.
- SPM underpins organizations' embrace of Agile planning.

##### Wave: Enterprise Collaborative Work Management, October 17, 2016
- Leaders: Clarizen, Redbooth, Wrike, Planview, Asana, and Smartsheet
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Enterprise+Collaborative+Work+Management+Q4+2016/-/E-RES121721)
- Criteria for inclusion: Combine collaboration and task management to organize planned and unplanned work; Ability to create work plans out of ad hoc conversations or individual activities; how are tools used to work with external users as well as internal.


### Gartner: 

##### Market Guide for Continuous Configuration Automation Tools 8 January 2018
- Companies included: Chef, CFEngine, Inedo, Orca, Puppet, Red Hat, SaltStack
- [Link to report](https://www.gartner.com/document/3843365)
- Definition: CCA tools are a programmable framework on which configuration and provisioning tasks can be codified, versioned and managed like any other piece of application code. Many of the tools in the market provide a repository to store and manage configuration content, but can be integrated with or use code revision control systems in use by application development teams.

##### Magic Quadrant for Software Test Automation, 20 November 2017
- Leaders: Micro Focus, Tricentis
- [Link to report](https://www.gartner.com/document/3830082)
- Gartner expectations: The market will remain very dynamic, creating opportunities for vendors over the next 12 to 18 months as significant investment capital continues to flow and commercial providers expand portfolios to better support agile and DevOps best practices.

##### Magic Quadrant for Application Performance Monitoring Suites, 19 March 2018
- Leaders: Cisco, Dynatrace, New Relic, CA Technologies
- [Link to report](https://www.gartner.com/doc/3868870)
- Gartner expectations: While AIOps for applications functionality is considered to be an essential component of a Magic Quadrant-qualifying APM suite, vendors whose offerings are focused on general AIOps capabilities are increasingly seen by users as viable alternatives to classical APM vendors. IT operations and other organizations are already awash with data, and this situation will likely only become more challenging with the growth of cloud, microservices, IoT and so on. Hence, Gartner sees companies such as Splunk, Sumo Logic and even the open-source Elastic Stack (combining Elasticsearch, Logstash and Kibana) as leveraging their current repository capabilities to act as the system of "truth" for APM data. 

##### Magic Quadrant for Application Security Testing, 19 March 2018
- Leaders: Micro Focus, CA Technologies (Veracode), Checkmarx, Synopsys, IBM
- [link to report](https://www.gartner.com/doc/3868966/magic-quadrant-application-security-testing)
- Gartner expectations: Although not strictly a security testing solution, SCA solutions have become critical components of application security programs. SCA products analyze application composition to detect components known to have security and/or functionality vulnerabilities or that require proper licensing. It helps ensure that the enterprise software supply chain includes only components that have undergone security testing and, therefore, supports secure application development and assembly. Gartner clients are increasingly seeking these capabilities from AST vendors. As such, vendors in this Magic Quadrant deliver SCA through homegrown solutions or partnerships with leading SCA vendors to supply analysis and governance capabilities to their clients.

##### Magic Quadrant for Project Portfolio Management, 29 May 2018
- Leaders: Planview, CA Technologies, Changepoint, Planisware
- [Link to report](https://www.gartner.com/document/3876967)
- Gartner expectations: Providers in this Magic Quadrant include those focused on general-purpose or enterprisewide PPM, as well as IT PPM, as long as the providers meet all of the required inclusion criteria. When evaluating and positioning the vendors included in this research, emphasis is placed on the importance of vendors offering multiple PPM products supporting different PPM use cases.

##### Enterprise Agile Planning Tools, 23 April 2018
- Leaders: CA, Atlassian, Microsoft, CollabNet
- [Link to report](https://www.gartner.com/doc/3872863)
- Gartner expectation: The enterprise agile planning (EAP) market is experiencing a disruption as mergers and acquisitions (M&As) occur and visionary new players enter. Vendors that rely upon legacy product strategies are becoming less relevant. Organizations are maturing in their use of enterprise agile and are demanding more functionality from their tools. Meanwhile, advances in machine learning promise new insights into the effectiveness of enterprise agile development. We expect that M&A activities will continue as providers seek to position more-complete DevOps toolchains in support of digital business and cloud platforms. Toolchain-based DevOps practices are the norm, and EAP tools are becoming a part of that toolchain. This places emphasis on the ability of EAP tools to integrate with and consume data from other tools in the chain for planning, reporting and analysis. 

##### Market Guide for Container Management Software, 9 August 2018
- Companies included: Alibaba Cloud, Cisco, DH2i, Diamanti, Docker, Google, HashiCorp, Heptio, IBM
- [Link to report](https://www.gartner.com/document/3886068)
- Definition: Container management software covers on-premises and/or hybrid systems that provide the automation and abstraction of containers along with infrastructure capabilities. Deploying containers at scale requires capabilities that enable agility for application developers, while also providing reliability and simplicity for I&O teams. Container management software systems provide not only abstraction of infrastructure details and application life cycle pipeline enablement for developers, but also an underlying infrastructure foundation that scales and is secured. In many cases, these solutions are allowing enterprises to use containers as a replacement for application servers.

##### Magic Quadrant for Enterprise High-Productivity Application Platform as a Service, 26 April 2018
- Leaders: Salesforce, OutSystems, Mendix, ServiceNow
- [Link to report](https://www.gartner.com/document/3872957)
- Gartner expectation: The long-term, sustained leadership in the market remains open to new players. New vendors continue to appear: Gartner counted 75 hpaPaaS vendors at the start of this research; now 85 and rising. Platform convergence through hpaPaaS vendors starting to support more-sophisticated business logic, business process and user experience capabilities, and competing against specialist vendors for these areas. Some of the vendors in the bpmPaaS market (such as AgilePoint, Appian and Pegasystems) and the RMAD market (such as Progress and Kony) are increasingly seeing themselves as competing with other hpaPaaS vendors for application development platforms. Some of these specialists are merging with hpaPaaS vendors through acquisition (for example, ServiceNow acquiring SkyGiraffe), while others find themselves competing for common hpaPaaS use cases.

##### Magic Quadrant for Data Science and Machine-Learning Platforms, 22 February 2018
- Leaders: KNIME, H20.ai, RapidMiner, SAS, Alteryx
- [Link to report](https://www.gartner.com/doc/3860063)
- Gartner expectation: A focus on operationalization, including the ability to manage models and collaborate at an enterprise level. This also includes the ability to orchestrate the complete analytical process, with a focus, ultimately, on productionizing and operationalizing models, increasing productivity and boosting business impact. The availability of free and open-source options that are often the first to offer innovative analytic capabilities. They also represent easy-to- access, low-initial-investment options for starting data science and machine-learning projects.