---
layout: markdown_page
title: "UX Research"
---

### On this page

{:.no_toc}

- TOC
{:toc}

## UX Research

The goal of UX Research at GitLab is to connect with GitLab users all around the world and gather insight into their behaviors, motivations and goals when using GitLab. These insights are then used to inform and strengthen product and design decisions.

### UX Researcher onboarding

If you are just starting out here at GitLab, welcome! Make sure to review all the pages here in the UX section of the handbook, they will help you get oriented. There is also a specific page dedicated to [UX Researcher onboarding](/handbook/engineering/ux/uxresearcher-onboarding).

### How to request research

Maybe you're interested in investigating [how notifications can be improved](https://drive.google.com/file/d/1-ZM0IsgkmOpmMTomKK62UNNhReMLY2p-/view), or you want to hear from [churned users](https://drive.google.com/file/d/15TnL_MbWDbPAKuIzt6dtzZrOEukeEzrV/edit) about their experiences with GitLab, then follow the steps below to request help from the UX Research team.

1.  First, check the [UX Research archive](/handbook/engineering/ux/research-archive) to see if there is any previous research available to address your question(s). If there isn't, proceed with the following:

1. Create a new issue using the `Research proposal` template in the UX research project and `@` mention the relevant UX Researcher, UX Designer, and Product Manager for the product stage. Ensure you answer all questions outlined in the `Research proposal` template.

    * You can find out who the relevant UX Researcher and/or UX Designer is by looking at the [team page](/team/) and filtering by the `UX` department. To find out who the relevant Product Manager is, take a look at the [Product category page](/handbook/product/categories/).

    * Anybody across GitLab can raise a research proposal, this includes UX Researchers.

1. The UX Researcher will review the issue and may respond with some follow-up questions. 

    * We want to lessen the time that researchers spend within issues and/or Slack soliciting research requirements. As a rule of thumb, if we have gone back and forth more than 3 times, it's time for a video call. 

    * Similarly, some features are more complex than others. A UX Researcher needs to fully understand the feature they are testing and why they are testing it. Sometimes, it's much easier to get a grasp on the feature's history, your existing plans and plans for the future when you talk with us. In cases such as these, the UX Researcher will schedule a kick-off call with all relevant stakeholders (including the UX Designer and Product Manager) to run through the research proposal. 

1. In collaboration with the Product Manager, the UX Researcher will determine the priority of the study and schedule it accordingly.

1. The UX Researcher will book a wash-up meeting with all relevant stakeholders when the results of the study are available. 


### Wash-up meetings

1.  During a wash-up meeting, all stakeholders collaboratively agree upon a list of improvements/actions to take forward. UX Researchers are responsible for scheduling wash-up meetings. At least 24 hours prior to the wash-up meeting, a Google document containing the research study’s key findings will be added to the meeting’s calendar invitation. 
1. In order to ensure that the meeting is an effective use of everybody’s time, you should familiarise yourself with the study’s key findings prior to the meeting and come prepared with suggestions.

### UX Research panel

The UX Research panel was established to gather feedback on how we can continue to improve GitLab. Participants in our research panel are the first to see new features and help drive product improvements for a better user experience.

To learn more about what it means to participate in UX Research at Gitlab, visit our [Research panel page](/researchpanel/). To sign up, please fill out this [survey form](https://gitlab.us3.list-manage.com/subscribe?u=195066e322642c622c0ecdde3&id=418480964e).

### UX research archive

The [UX research archive](/handbook/engineering/ux/research-archive) is a collection of UX Research broken down by specific areas of GitLab. It is intended to shed insight into key UX design decisions and the data that informed those decisions.

As the UX Department conducts and completes research, it will be added to the UX research archive. The goal of the archive is to surface and share our findings with all of GitLab and the community.

## UX Researchers

### How we decide what to research

UX Researchers are encouraged to sit in on monthly planning calls for their relevant product stages.  These meetings keep them informed about what Product feels are the most important initiatives at GitLab. UX Researchers should offer ways in which they can assist in the delivery of such initiatives. 

### Working on a research study

1. Update the `Research proposal` with the following:
    * Label the issue with the area of GitLab you’re testing (for example, `navigation`), the status of the issue (`in progress`) and the Product stage (for example, `manage`).
    * Add a milestone to the issue.
    * Mark the issue as `confidential` until the research is completed so it doesn’t influence user behavior.
    * Assign the issue to yourself.
    * Add a checklist of actions that you plan to undertake. This makes it easier for people to understand where the research is up to.
    * Add related issue numbers.
1. Conduct the research. Ensure you keep the checklist up-to-date.
1. Schedule a wash-up meeting. Ensure you attach the study's key findings to the calendar invitation at least 24 hours prior to the meeting.
1. Record and facilitate the wash-up meeting.
1. After the wash-up meeting, undertake any actions you have agreed to take forward, such as creating new issues.
1. Document the study within a report. The report should consist of:
    * Research hypotheses
    * Research methodology
    * Findings
    * Next steps/recommendations as agreed upon in the wash-up meeting.
1. Update the `Research proposal` with the following:
    * Add the report and any supporting evidence (such as user videos).
    * Unmark the issue within the UX research project as `confidential`. (In some cases the issue may need to remain confidential if sensitive information is shared. If you’re unsure of whether an issue should remain confidential, please check with Sarah O’Donnell `@sarahod`).
    * Update the status of the issue to `done`.
    * Close the issue. You should stay assigned to closed issues so it's obvious who completed the research.
    * Add the report to the [UX Research Archive](/handbook/engineering/ux/research-archive) and label as appropriate.


[ux-guide]: https://docs.gitlab.com/ee/development/ux_guide/
[ux-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX
[ux-ready-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX+ready
[gitlab-design-project-readme]: https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md
[twitter-sheet]: https://docs.google.com/spreadsheets/d/1GDAUNujD1-eRYxAj4FIYbCyy8ltCwwIWqVTd9-gf4wA/edit
