---
layout: markdown_page
title: "Throughput"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

[Throughput](https://weblogs.asp.net/wallen/throughput-vs-velocity), is a
measure of the total number of MRs that are completed and in production in a
given period of time.  Unlike velocity, throughput does not require the use of
story points or weights, instead we measure the number of MRs completed by a
team in the span of a week or a release.  Each issue is represented by 1
unit/point.  This calculation happens after the time period is complete and no
pre-planning is required to capture this metric.  The total count should not be
limited to only MRs that deliver features, it's important to include
[engineering proposed](/handbook/engineering/#engineering-proposed-initiatives)
MRs in this count as well.  This will ensure that we properly reflect the team's
capacity in a consistent way and focus on delivering at a predictable rate.

## Implementation

Each merge request must have one of the following labels:

- ~bug
- ~"feature proposal"
- ~security
- ~backstage
- ~"Community contribution"

If it does not have any of these, it will be tracked in the 'undefined' bucket
instead. The Engineering Manager for each is ultimately responsible for ensuring
that these labels are set correctly.

Throughput charts are available on the [quality dashboard] for each team.

[quality dashboard]: http://quality-dashboard.gitlap.com

## Why we adopted this model

- The goal for using this measure is to incentivize teams to break MRs to the smallest deliverable which lead to a smaller set of changes and the many benefits that come along with that.
- This practice aligns with one of our core values: [Iteration](/handbook/values/#iteration), do the smallest thing possible and get it out as quickly as possible.
- Instead of spending time sizing and figuring out the weight of an issue, we should put this effort toward breaking issues to the smallest [deliverable](/handbook/engineering/#code-quality-and-standards).
- Since throughput is a measure of actual work completed, it is far more
accurate than using weights.
- Throughput is a simpler model to implement for new teams since the measure
  is the count of small well defined MRs.
- Unlike weights which may be estimated differently from one team to another,
  throughput can be normalized across every engineering team.

## A few notes to consider when using this model

- There are many activities such as code reviews, meetings, planning that we do not count as units of work independently, they are however accounted for as part of the delivery of an issue whether it be feature work or technical debt.  The team's rate of delivering code to production is what we are trying to measure.
- This metric is a tool for an Engineering Manager to [determine the capacity
  of the team](/handbook/engineering/management/#project-management) for each given release.
- While there is no scoring required with this model, there is still value
  for an Engineering Manager to look through each issue they are committing to
  in any given release and making sure they are well defined small deliverables.

When combined with cycle time, throughput is a great metric to help you identify areas of improvement and possible bottlenecks that the team can work to address.
