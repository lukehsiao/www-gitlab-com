---
layout: markdown_page
title: "Interviewing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## How to Apply for a Position

The best way to apply for a position with GitLab is directly through our [jobs page](/jobs), where our open positions are advertised. If you do not see a job that aligns with your skillset, please keep your eye on the jobs page and check back in the future as we do add roles regularly. Please be advised that we do not retain unsolicited resumes on file, so you will need to apply directly to any position you are interested in now or in the future.

To apply for a current vacancy:

1. Go to our [jobs page](/jobs) and [view our open opportunities](/jobs/apply)
1. Click on the position title that interests you! Please also refer to the [country hiring guidelines](/jobs/faq/#country-hiring-guidelines) to see if we are able to hire in your location.
1. You will be redirected to the vacancy description and application form, where you will be asked to fill out basic personal information, provide your resume, LinkedIn, and/or cover letter, and answer any application questions, as well as answer a voluntary Equal Employment Opportunity questionnaire. While the EEO questionnaire has `US` in its title, it's open to all applicants from around the world.
1. Once you have finished, click "Submit Application" at the bottom.
1. Should you reach out to any GitLabbers on LinkedIn instead of or in addition to applying to our jobs page, you'll receive the following reply: "Thank you for your interest in GitLab. We would much rather prefer you apply for the position you have in mind directly via our [Jobs page](https://about.gitlab.com/jobs/). This will ensure the right GitLabber reviews your profile and reverts back to you! Unfortunately at this time, I can not refer you for the position as we have not had a chance to work together. To ensure we stay [Inclusive](https://about.gitlab.com/handbook/values/#diversity), I can also not influence your application".

## Typical Hiring Timeline

A candidate is welcome to contact the recruiting team at any time for an update on their candidacy. These steps may vary role-to-role, so please review the hiring process per vacancy.

1. Prior to interviewing, the recruiting team will utilize our Applicant Tracking System (ATS), [Greenhouse](https://www.greenhouse.io/), to identify the most qualified candidates for the vacancy. The hiring team will also source for candidates that may not be actively looking. There are many factors to consider when reviewing resumes. Some of those factors can be aided through technology within the ATS, others require human eyes to evaluate the qualifications. There are several posts that reveal suggestions for reviewing resumes that our team may utilize. [Greenhouse](https://www.greenhouse.io/blog/in-review-whats-the-right-way-to-read-a-resume), [Zip Recruiter](https://www.ziprecruiter.com/blog/10-crucial-things-to-look-for-in-a-resume/) and [The BalanceCareers](https://www.thebalancecareers.com/gone-in-thirty-seconds-how-to-review-a-resume-1919139) are three examples.
1. The employment team does a **first round of evaluations** by reviewing candidate resumes. The employment team will also refer to the [country hiring guidelines](/jobs/faq/#country-hiring-guidelines) before moving candidates forward. Disqualified candidates will be sent a note informing them of the [rejection](#rejecting-candidates). There are templates in Greenhouse to assist, but messages can be tailored as appropriate. Make sure the message is professional and respectful.  
1. **Pre-screening Questionnaire**: Some candidates will be sent a pre-screening questionnaire by the employment team relating to the position to complete and return to the sender. The questionnaire and answers are kept within the candidate's Greenhouse profile.
   1. Team members who review the pre-screening questionnaire answers should refer to the [GitLab private project](https://gitlab.com/gitlab-com/people-ops/applicant-questionnaires) that holds guides on how to review each of the questionnaires. Candidates who receive an assessment are moved to the "Assessment" stage in Greenhouse by a member of the Recruiting team and sent the questionnaire. The recruiting team also chooses a member of the hiring team to review the responses once they are submitted.
   1. When a candidate returns their assessment, the recruiting team member who sent the assessment and the hiring team member who was chosen to review the assessment will receive a notification. Once a reviewer submits the feedback for the assessment in Greenhouse, the recruiting team will be notified.
   1. Candidates that have satisfactory assessment results may be invited to a screening call. Disqualified candidates will be sent a note informing them of the rejection.
1. [**Screening call**](#screening-call):
   1. If the candidate qualifies, a member of the hiring team will conduct a screening call using Zoom and scheduling it via Greenhouse.
   1. A member of the employment team will move the candidate to the "Screening" stage in Greenhouse. They will reach out to the candidate to collect their availability and then send out calendar invitations to both the interviewer and candidate.
   1. Our [recruiters](/job-families/people-ops/recruiter/) will do a screening call;
depending on the outcome of the screening call, the recruiting team or manager can either [reject a candidate](#rejecting-candidates) or move the candidate to the team interview stages in Greenhouse.
   1. The recruiter will wait 5 minutes for the candidate to show up to the appointed video call link, which is always shared with the candidate via email. If the candidate does not show up to the interview or reach out in advance to reschedule, the candidate will be classified as a "no show" and be disqualified.
   1. The recruiter, hiring manager, or candidate can terminate the discussion early at any point during the interview if either party determines that it isn’t a fit. Be as transparent and honest as possible and provide feedback.
   1. After the screening call, the recruiter will verify that the candidate is not on any known [Denied Party List](https://www.export.gov/csl-search). If the candidate is on a list, the application process will end.
1. **Behavioral interview**: Some roles include a behavioral interview with a team peer or leader. Behavioral interviews may be conducted as [panel interviews](/handbook/hiring/interviewing/panel).
1. **Technical interview (optional)**: Certain positions also require [technical interviews](/handbook/hiring/interviewing/technical).
1. **Further interviews**: All interviewers will assess the candidate's values alignment, by asking behavioral questions and scoring the values alignment as part of their feedback form in Greenhouse. Additional interviews would typically follow the reporting lines up to the CEO. For example the technical interview may be conducted by an individual contributor, with subsequent interviews being conducted by the manager, director, executive team member, and then potentially the CEO. The candidate should be interviewed by at least one female GitLab team member. The GitLab team understands the importance of Inclusive interviewing. We thrive to ensure our hiring team is well versed in every aspect of Diversity, Inclusion and Cultural competence. A positive and comfortable candidate experience is priority. Interviewers will follow the same "no show" policy as the recruiters; if a candidate does not show up or reach out to the team, they will be disqualified. All interviewers will complete interviewing training, which will be assigned to them from the recruiting team and can be found in the [People Ops Employment issue tracker](https://gitlab.com/gitlab-com/people-ops/employment/issues).
1. **CEO interviews**: The CEO might want to interview candidates in a last round interview. However, director level and above candidates will always interview with the CEO for at least 90 minutes.
2. **References**: The hiring manager or the hiring team will contact [references](#reference-check-process) for promising candidates. References will be collected towards the end of the interview stage for final candidates. References must be checked before an offer is made. Three references will be asked for but at minimum two references need to be completed, and at least one needs to be a manager. The recruiting team will move the candidate to the "Reference Call" stage in Greenhouse, and email the candidate to request their references' contact details. After the reference checks are completed, the person performing the reference check will input a scorecard in Greenhouse with their findings.
1. **Offer package**: After reference calls are completed successfully, the recruiting team moves the candidate to the "Offer" stage and submits the [offer package](#offer-package) in Greenhouse for appproval from the People Business Partner, executive of the division, and Chief Culture Officer for approval.
1. The recruiter, hiring manager, executive, or CEO may make an **offer** verbally during a call with the candidate, but it will always be quickly followed by a written offer email prepared by the recruiting team and an official contract as described in [the section on preparing offers and contracts](#prep-contracts).
1. The recruiting team will, if applicable, add language to the contract that states that employment or engagement is contingent on a valid work permit or visa. A start date should factor in that the approval of a new work permit may take several weeks.
  * Note that, when scheduling a start date, People Ops requires at least 3 days notice from the receipt of an executed offer until the GitLabber's proposed first day. Unless it's not possible (e.g. due to a conflict with a public holiday), the new hire should also be scheduled to start on a Monday in order to ensure the best possible onboarding experience.
1. The manager follows up to ensure that the offer is accepted and that the contract is signed.
1. The recruiting team [starts the background check process](/handbook/people-operations/code-of-conduct/#background-checks) if applicable for the role.
1. People Ops [starts the onboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md).
1. Manager considers [closing the vacancy](/handbook/hiring/vacancies/#closing-a-vacancy).

## Screening Call

We conduct screening calls for all positions. This call will be completed by our [recruiters](/job-families/people-ops/recruiter/). Calls can last anywhere between 10 and 30 minutes depending on the conversation.

Example questions include:

1. Why are you looking for a new position?
1. Why did you apply with GitLab?
1. What are you looking for in your next position?
1. Why did you join and leave your last three positions?
1. What is your experience with X? (for each of the skills listed in the position description)
1. STAR questions and simple technical or skills-related questions
1. What is your current location and do you have any plans to relocate? (relevant in context of compensation, country-hiring guidelines, and in case an offer would be made)
1. Do you require visa sponsorship or a work permit to work for GitLab?
1. What is the notice period you would need if you were hired?
1. What are your compensation expectations?

At the end of the screening call, the candidate should be told what the next steps would be and the timeline.
An example message would be "We are still reviewing applications, but our goal is to let you know in 5 business days from today whether you've been selected for the next round or not. Please feel free to ping us if you haven't heard anything from us by then."

## Moving Candidates Through The Process

In an effort to streamline the hiring process, improve the candidate experience, and hire talent faster, best practices are to conduct interview days so candidates can complete the process within 2 days. For example, just as if we were to interview candidates in-person at an office, we wouldn’t make them come back 3, 4, or even 5 times to interview. The initial screening call and optional CEO are not considered to be part of the 2 day goal. Those on the interview team should prioritize the interview in their schedule.

If it means you have to miss an already scheduled or recurring meeting, please consider watching a recording or reviewing notes from the agenda instead of attending so that you can participate in the interview. Hiring an amazing team is critical for GitLab and how we spend our time shows where our priorities are.

**Maintain candidate confidentiality.** All candidate names and details are kept confidential within the hiring team, to avoid bias or the potential to jeopardize a candidate's current employment, as well as to maintain data protection. The only people who should have access to details about candidates are Recruiting, People Ops, the hiring manager(s), approved interviewers or reviewers within that team, the executive of the department, the legal team, the CFO, and the CEO. The only caveat to this is if a team member refers a candidate, or the candidate intentionally reaches out to someone at GitLab, and even then the team member will only know their name, that they are interested in GitLab, and if they are a referral what stage they are in. Anytime you want to discuss a current, past, or potential candidate, please do so privately (whether in a private Slack channel/message, email, or within Greenhouse). If you have access to it, you can also provide the direct Greenhouse link and avoid mentioning names or identifying details. Emails from the candidate are synced on our ATS, and for that reason the entire hiring team for that position have access to it. Remember to ensure any sensitive information is marked as secret/private in the candidate profile.

**Remember to inform candidates about what stage they are in.** For example, if in the hiring process for the particular position / team you've agreed that there will be four stages, be sure to inform the candidate of where they are in the process during each call / stage: "You are in stage X and will be moving to stage Y next." Some brief feedback from the previous stage can also be included to help the candidate gauge their progress. If there will be additional or less stages than expected, be sure to let the candidate know so they are aware of where they are in the process.

**The process can differ from team to team and from position to position.** If a candidate submits a resume to a particular open position and is being considered for another open position, send a short note to update and approve with the candidate, as well as inform them that their process may be slightly different or delayed. If the roles are on different teams, the candidate will ideally only move forward with one, depending on their interests and qualifications. If the candidate is being rejected for one or all of the positions they applied for, they will be notified of which vacancies they are being rejected for.

**Recruiters will schedule the next person in the process.** Someone on the recruiting team will move candidates forward to the next person in the hiring process if the candidate has received positive feedback.

**Compensation is discussed at start and end but not in between.** Compensation expectations are asked about during the [screening call](#screening-call). If the expectations seem unworkable to the manager or recruiter (based on what had been approved by the compensation committee at the [creation of the vacancy](/handbook/hiring/vacancies/#vacancy-creation-process), then the recruiter can send a note to the candidate explaining that salary expectations are too far apart, but they should also ask how flexible the candidate is and if they would consider adjusting their expectations. If expectations are aligned, then the topic of compensation should not re-surface until an [offer is discussed internally](#offer-authorization). Following this guideline avoids conflating technical and team interviews with contract discussions and keeps the process flowing smoothly.

If the manager has a question about compensation, please ping the People Ops Analyst for review. If the question needs to be escalated, the People Ops Analyst will add the Chief Culture Officer to the conversation.

**An approval team authorizes all offers.** The manager proposes a suggestion for an offer (including bonus structure if applicable, etc., using the [global compensation framework](/handbook/people-operations/global-compensation)) as a private comment in Greenhouse and informs the recruiting team on its details depending on what is applicable. The recruiting team will create an [offer package](#offer-package) to present to an approval chain, consisting of the People Business Partner, executive of the division, and Chief Culture Officer, for approval. Verbal offers should not be extended to the candidate until the offer is approved. The CEO may choose to interview the candidate, and any offers given before their approval are premature.

## Conducting a GitLab Interview

Interviewing is hard for both sides. In less than one hour you both need to get to know each other and both will have to make the decision if you want to work with this person.
This is an effort to provide a set of guidelines to make interviewing a bit less traumatizing.

Note: So you are about to interview folks for a job at GitLab? Please take a moment to carefully read
[this document on keeping it relevant and legal, including a self-test](https://docs.google.com/document/d/1JNrDqtVGq3Y652ooxrOTr9Nc9TnxLj5N-KozzK5CqXw) (please note this document is internal to GitLab while we edit it to make it fit for general audiences). For example, if there is a gap in employment history on a CV, you can ask the candidate what they did during that time to keep their skills current. You may not ask why they were absent from work as it may be related to a medical or family issue which is protected information.

When discussing the interview process with candidates, it is encouraged to set context for the interview (such as providing links to the handbook and if the interview will be a behavioral or technical interview, etc.), but GitLabbers should not prep candidates for specific interview questions.

New internal interviewers will partake in [interviewing training](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/interviewing.md), which will be assigned by the recruiting team. As part of the training, team members will shadow an interviewer and be shadowed by one, in order to make sure all GitLabbers are following our interviewing process and creating an excellent candidate experience. The interviewer who will work with the team member should be aligned to either their timezone or the role they'll be helping interview for. Feel free to ping `@gl-hiring` in your training issue if you are not sure which interviewer to contact, or send a message in the `#hiring` channel in Slack.

### Before The Interview

* Screening - writing a good resume is an art, and not many people master it. When you read a resume look for evolution rather than buzzwords, and, if something sparks your curiosity, ask.
* If the process is taking too long, apologize and explain what is going on. It is really frustrating to not hear anything from the other side, and then continue conversations like nothing has happened. Show respect for the candidate's time.


### During The Interview

1. As candidates move through the interviewing process, interviewers take notes within our Applicant Tracking System, Greenhouse. As they move through the process, other interviewers have the opportunity to review any specific notes previous interviewers have left for them. Interviewers are not able to see the full feedback notes from previous interviewers in an effort to avoid bias. Meanwhile. hiring managers, executives, and people ops are able to see all feedback notes at any time.
1. There is an unbalanced power relationship during the interview. The interviewer is in a powerful position. They will decide if this candidate will move forward or not. Be mindful of this. Be as friendly and approachable as you can. Be frank about what is going on, explain how the interview is going to be and set clear expectations: tell it like it is. This has the added value of getting people comfortable (over time) and allows you to get much better data.
1. Communication is really hard, so don't expect perfect answers. Every person is different and they will express things differently. They are not listening to your train of thought so they may say things differently than what you expect; work on interpreting what they are trying to say rather than demanding them to explain it to you. Once you have an answer, validate your assumptions by explaining what you understood and allow the candidate to correct your understanding of the story.
1. Don't go checking for perfect theoretical knowledge that the interviewee can google when needed, or give a problem that took you 2 months to dominate yet you expect your interviewee to master in a 30 minutes conversation. Be fair.
1. Aim to know, at the end of the interview, if you want to work with this person.
1. Interview for soft skills. Really. Do it! Pick some behavioral questions to get data on what the candidate has done before and how their behavior aligns to the company values. We are all going to be much happier if we all naturally agree on how things should be. You will be asked to evaluate how the candidate aligns to our values in your feedback form, and asking behavioral questions is the best way to assess this.
1. Consider having more people interviewing with you, as different people see and value different things. More data helps you make better decisions and is a better use of interview time for both the candidate and the company.
1. Always let the interviewee ask questions at the end, and be frank in your answers.

### Considerations for interviews with technical applicants

1. Try to get a real sample of work (which we already do for developers by working on GitLab issues). Avoid puzzles or weird algorithm testing questions. Probing for data structures is fine as long as it is relevant to the job the person is going to do.
1. Be mindful of the background of the candidate, someone who knows 10 languages already (and some languages in particular, Perl for example), may pick up Ruby in a second given the right chance. Don't assume that someone with a Java background will not be capable of moving to a different stack. Note that individual positions may have stricter requirements; the Backend Engineer position [requires Ruby experience](/job-families/engineering/backend-engineer/), for example.
1. Consider including non-engineering people to ask soft skills questions. Because technical people should be capable of talking to non-engineering people just fine, we should assess the candidate's ability to do so.

### Candidate Performance Evaluation

The goal of behavioral questions is to get the candidate to share data on past experiences. Previous behavior is considered the most effective indicator of how a person is going to act in the future. It is important to remember that skills and knowledge can be learned easier than habitual behaviors can be changed, especially when candidates are unaware of the impact of the undesired behaviors.

The questions are usually in the form of: "Can you tell me about a time when...". The kind of answer that we are looking for is to get a story that is structured following the Situation, Task, Action, Result (STAR). Ask for an overview, an executive summary, of the case at hand. Avoid lengthy answers from the candidate at this stage.

Some things to pay attention to:

* What the candidate chose to highlight in the case as important
* Is it clearly explained? Is the story well told? If it is a technical story and the interviewer is a non-technical person, are things being explained in a way that is easy to understand?
* Is there a result or was the story left unfinished? Is it still going on?
* Was the result measured in any way? How does the candidate validate the result matched the expectation? Was there an expectation set to begin with?

There is no right answer, what matters here is to hear the candidate and gather data on how they are telling the story.

Once you have your notes, tell the candidate what you understood, repeat the story, and let them correct you as needed.

After having a high-level understanding of the case, we will want to dive deeper into the details. The objective of this step is to understand and detail the exact contributions a candidate has made to an effort which led to results. We will take a reverse approach to the STAR question structure presented earlier.

The key to analyzing each of the reverse-STAR steps is to ask _What, Why, How and Who_ at each step of the process. This will let the candidate paint a very clear picture of the situation, their ownership of the idea/solution, and their decision process in key pivotal moments. Reverse the order of the STAR structure and drill up from results to the situation as a whole. Find the answer to the following questions:
1. What was the goal to achieve or the problem to overcome? What was the expectation? Was the goal defined from the get-go?
1. How was the result measured? Why was it measured that way?
1. What steps or process was followed to achieve the result? List them together with the candidate
1. Who else was working with the candidate? Was the candidate working alone?
1. What role did the candidate have in the team, if not alone on the project? Was the candidate in charge of specific tasks? Who decided on task assignment? What was their impression of the tasks? How were the tasks decided on?
1. For the tasks discussed above, understand if there were resources who helped the candidate and at what capacity. How were those chosen and why?

These questions can be quite unbalancing and can increase the stress during the interview. Again, be kind and help the candidate understand what are you looking for, provide an example if one is needed when you notice the candidate is blocked.

It can also happen that the candidate does not have a story to share with you, that is okay. It's just another data point that should be added to the feedback (I failed to get data on ...); just move to the next question and be sure to have a few questions as a backup.

These questions should be aligned with our company values. What we are looking for is understanding how this candidate behaves, and if this behavior matches the one we look for in our company values. There is a scorecard in the feedback forms in Greenhouse to assess the candidate's values alignment when interviewing; please reach out to the recruiting team with any questions.

### Interview Feedback

Always leave feedback, this will help everyone to understand what happened and how you came to your decision. In Greenhouse, you will use an "interview kit" when interviewing a candidate, which has text for feedback and scorecards for skills and values. The bottom of the feedback form will ask for an overall recommendation on if you want to hire this person or not; please do leave a score for each candidate, and read our [handbook page discussing the scorecards and best practices](https://about.gitlab.com/handbook/hiring/greenhouse/#scorecards).

Scoring can be defined as follows:

  - `Strong Yes` - Very likely to hire (meets most requirements, aligns with values)
  - `Yes` - Semi-inclined to Hire (may meet some requirements, has some yellow flags)
  - `No` - Not likely to hire (meets few requirements, has many yellow flags, may not align with values well)
  - `Strong No` - Would not hire (does not meet requirements, red flags, not aligned with values)

## Rejecting Candidates

1. At any time during the hiring process the candidate can be rejected.
1. The candidate should always be notified of this. The employment team is primarily
responsible for declining candidates. The hiring manager should be prepared to let the candidate know why they were declined if they candidate had progressed to the team or manager interviews. The hiring manager can also share this feedback with the recruiting team, who will relay it to the candidate.
1. We only provide feedback for candidates who have passed the first interview stage and met with the team or hiring manager. If the candidate asks for further feedback, only offer frank feedback. This is hard, but it is part of our [company values](/handbook/values).
    * All feedback should be constructive and said in a positive manner. Keep it short and sweet.
    * Feedback should always be applicable to the skill set of the position the candidate applied and interviewed for.
    * Feedback and rejection should always be based on the job requirements.
    * If you feel uncomfortable providing feedback for whatever reason, reach out to the recruiting team for assistance.
    * Suggested feedback format: "The reason we don't think you're the best match for this position is __ . We are impressed with your skill in __ . That we decline you doesn't mean that you are not a good fit for this position. We receive over 1000 applications per month and have to decline almost all candidates. Both Facebook and Twitter rejected the founder of Whatsapp https://thehustle.co/whatsapp-founder-got-rejected-by-both-twitter-and-facebook-before-19-billion-buyout. We don't think we'll do any better and look forward to hearing from you after landing a better job or starting a successful company. Thanks for your interest in working at GitLab." This format can be used as a guideline to help candidates understand our decision, but can be personalized/customized to fit each situation. Personalization in communication with candidates is encouraged.
1. If people argue with the feedback that we provided:
    * Do not argue with or acknowledge the validity of the contents of the feedback.
    * Share their feedback with the people involved in the interviews and the decision.
    * Template text: "I've shared your feedback with the people involved in the interviews and the decision. We do not expect to revert the decision based on your feedback. In our hiring process we tend to error on being too cautious. We rather reject someone by mistake than hire someone by mistake, since a wrong hire is much more disruptive. Organizations can reject people with great potential http://thehustle.co/whatsapp-founder-got-rejected-by-both-twitter-and-facebook-before-19-billion-buyout so please don't be discouraged from seeking a great job."
1. The employment team may send out an inquiry to candidates to gather feedback after they have exited the hiring process.
   * The recruiting team will review all feedback and use it to improve the hiring process.

## Candidate Experience

We recorded a training on this subject:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/ng_VQseo5vo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## CEO Interview Questions

The questions are available in a [Google form](https://docs.google.com/forms/d/1lBq_oXaqpQRs-SeEs3EvpxFGK55Enqn_nzkLq2l3Rwg/viewform) which can be used to save time during the actual interview.
All candidates are asked to fill out the form when they are scheduled for an interview with our CEO to discuss during their call with the CEO.

## Reference Check Process

**Why Should Hiring Managers Check References**

We recorded a training on this subject here:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8pdf_rRihcE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

 - To ensure we are hiring the right candidate.
 - To understand under what circumstances this candidate thrives vs what circumstances frustrates them.
    - As the hiring manager, you will be the closest to the GitLabber and benefit most from learning about them.
-  To build your network.
    - As a hiring manager, you need to build a network for great talent.  Each reference you talk to can be a part of that network.
- To use the opportunity to spread the GitLab story.  We could spark new customers and new GitLabbers.

All GitLab hiring managers should be making the best effort to complete and conduct the reference checks for their candidate. As part of our hiring process we will ask candidate to provide us with three references to contact (at least one should be a manager). At a minimum two references should be completed. When a candidate passes the initial screening and first rounds of interviews but before they advance to meet with senior leadership, the hiring manager should reach out and contact the references provided. If it is not possible to schedule a call via phone, a hangout or zoom meeting can also be arranged if it's convenient. If that is not possible an email can be sent with the following questions:

- Can you briefly describe your working relationship with them?
- What are they like to work with?
- What can they improve upon?
- What advice would you give their next hiring manager?

You can also elaborate further and ask follow-up questions if the opportunity arises. The hiring team will be adding engineering questions into Greenhouse so that all engineering hiring managers have access to the same questions. Additionally, the hiring team will work with each function to identify any other specific questions hiring managers would like to add to Greenhouse for their team.

You should not ask any questions about the person's race, gender, sexual preference, disabilities or health, political affiliations, religion, family (children). Example questions not to ask:
- Who watches their children while they are at work?
- What types of groups does the candidate belong to that are not work related?

All reference check feedback should be entered into Greenhouse using the Reference Checks scorecard. To add this information, go to the candidate's profile, make sure they are in the "Reference Checks" stage, and click "Collect Feedback".

It is the hiring manager's responsibility to do the reference checks but the hiring team can also provide assistance and guidance. You can also refer to [these guidelines](http://www.bothsidesofthetable.com/2014/04/06/how-to-make-better-reference-calls/). Increasingly, organizations have a company policy that prevents their employees from providing references; instead, they are only able to verify employment, including dates of employment and title. Do not judge a candidate because his or her former employer has this policy; it does not mean the candidate was not successful. Instead, go back to the candidate to get the name and contact information for an alternative reference.

The recruiting team may also ask candidates if there is anyone who they've worked with in the past who currently works at GitLab or knows someone at GitLab that we could talk to.

## Offer Package

The employment team will create an offer package for each candidate in Greenhouse after their interview with an executive and their reference checks are completed.

To create the offer package, move the candidate to "Offer" in Greenhouse and select "Manage offer". Input all required and relevant information, ensuring its correctness, and submit, then click `Request Approval`. Please note that any changes in compensation packages will result in needing re-approval from each approver.

Note that the hiring package should include the candidate's proposed compensation in the most appropriate currency and format for their country of residence and job role. Annual and monthly salaries should be rounded up or down to the nearest whole currency unit and should always end with a zero (e.g., "50,110.00" or "23,500.00"). Hourly rates should be rounded to the nearest quarter-currency unit (e.g., 11.25/hr.).

For internal hires, be sure to include in the "Approval Notes" section what the candidate's current level and position, as well as compensation package.

You can also include any mitigating circumstances or other important details in the "Approval Notes" section of the offer details. If the comp has a variable component please list base, on target earnings (OTE), and split in the "Approval Notes".

Please make sure that the level and position match the role page.

In case it is a public sector job family please note (the lack of) clearances.

Information in the offer package for counter offers should include the following in the "Approval Notes" section:

   - New offer:
   - Original offer:
   - Candidate's salary expectation beginning of process:
   - Candidate's counter offer:

Anyone making comments regarding an offer should make sure to mention the recruiter and hiring manager.

The People Business Partners and People Ops Analyst will receive an email and/or Slack message notifying them of the offer. The People Business Partner for the department will either approve or deny the request, and the People Ops Analyst will ensure the compensation is in line with our compensation benchmarks. Only one approval is needed in order to move forward. Once approved, the executive of the division will then receive a notification to approve. Once approved, the CEO and Chief Culture Officer will receive a notification to approve; only one approval is required in order to move forward with the offer. Typically, the Chief Culture Officer will provide the final approval, but if the CCO is out of office, the CEO will be the final approver.

It is recommended to also ping approvers, especially the executive (and CEO if needed) in Slack with the message "Hiring approval needed for [Candidate Name] for [Position]" with a link to the candidate profile. To create the link, search for the candidate in Greenhouse, select the candidate, go to their offer details page, and copy the link. Do not copy a link from a different section of their candidate profile.

Once the hiring package has been approved by the approval chain, the verbal offer will be given, which will be followed by an offer email and official contract, both of which are sent through Greenhouse. If there is special compensation or bonus as part of the offer package, the CFO or Senior Director of Legal will need to approve the language before proceeding with the contract.

The Director of Recruiting will sign all contracts before they go to the candidate to sign. If the Director of Recruiting is out of office, the CCO will sign.

## Getting Offers and Contracts Ready, Reviewed, and Signed
{: #prep-contracts}

Offers made to new team members should be documented in Greenhouse through the email thread between the person authorized to make the offer and the candidate.

1. Email example is in the "Offer letter" template in Greenhouse. When using the template:
   1. make sure that you offer the correct [contract type and entity](/handbook/contracts/#how-to-use), ask People Ops if in doubt;
   1. include both the People Ops and Recruiting aliases in the cc (this should be the default), as well as the hiring manager and executive of the role, and
   1. be sure to send the email from Greenhouse by going to the candidate profile, clicking "Email [Name]" and selecting the appropriate email template.
   1. Note: the number of proposed stock options must always be mentioned specifically, even when it is 0.
1. One person from the recruiting team will follow up with the contract. They will:
   1. check all aspects of the offer:
      - was it approved by everyone?
      - do the contract type and entity make sense?
      - is it clear how many (if any) stock options this person should receive?
      - is all necessary information (start date, salary, location, etc.) clearly available and agreed to?
      - does the candidate need a work permit or visa, or require an update to them before a start date can be agreed?
   1. generates the contract within Greenhouse based on the details found in the offer package
   1. uses reply-all on the offer email to gather any missing pieces of information,
   1. if the contract was created outside of Greenhouse, has the contract reviewed and approved by a People Business Partner in Slack as a quality check,
   1. stages the contract in DocuSign from within Greenhouse, which emails the contract to the signing parties, with people ops, recruiting, and the hiring manager cc'd.
1. When the contract is signed, the recruiting team member should mark the candidate in Greenhouse as "Hired". Thanks to an integration between Greenhouse and BambooHR, it will automatically add an entry for the new team member in BambooHR. However, in the automatic move, "self-service" is switched off in BambooHR by default, so this must be switched on explicitly within BambooHR. The recruiting team member will ensure that the "hired" date in Greenhouse matches the date the contract was signed and close the vacancy in Greenhouse if necessary.
1. People Operations will then file the signed contract in the appropriate place and start the [onboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md).
1. Candidates will start the onboarding process no more than 30 days before her/his start date.

## Background Checks

Team members in the certain positions must partake in a [background check](/handbook/people-operations/code-of-conduct/#background-checks), which covers criminal and employment history.

Candidates who are in Support Engineering, Customer Success, People Ops, Finance, Sales (client-dependent), and the Executive team will be sent the link to the background check after the contract is signed. The new team member may start with GitLab prior to the returned check, but their continued employment will be contingent on a clear returned check.
