---
layout: markdown_page
title: "Atlassian Crucible"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Challenges
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary

Atlassian Crucible is a collaborative code review application. Like other Atlassian products, Crucible is a Web-based application primarily aimed at enterprise, and certain features that enable peer review of a codebase may be considered enterprise social software. Crucible is particularly tailored to distributed teams, and facilitates asynchronous review and commenting on code. Crucible also integrates with popular source control tools such as Git and Subversion. Crucible is not open source, but customers are allowed to view and modify the code for their own use.
(derived from [Crucible wikipedia page](https://en.wikipedia.org/wiki/Crucible_%28software%29))

Like Crucible, GitLab provides code review features, and also is optimized to help large (and small) teams work asynchronously. In addition to code review capabilities, GitLab also provides a Git based source code repository, issue tracking and management, CI/CD built-in, security testing, packaging, release, configuration, and monitoring, all within a single application covering the entire DevOps lifecycle.

## Resources
* [Atlassian Crucible website](https://www.atlassian.com/software/crucible)
* [Crucible Wikipedia](https://en.wikipedia.org/wiki/Crucible_%28software%29)

## Pricing
* [Pricing page](https://www.atlassian.com/software/crucible/pricing)
* Small teams
  - $10 US - one-time payment, unlimited repos, up to 5 users
* Growing Teams
  - one-time payment, unlimited repos
    - $1,100 US - up to 10 users
    - $1,650 US - up to 25 users
    - $3,030 US - up to 50 users
    - . . .
    - $13,200 US - up to 500 users
    - $17,600 US - up to 2000 users
    - $22,000 US - 2000+ users

## Comparison
